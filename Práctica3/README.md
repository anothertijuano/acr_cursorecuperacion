# Práctica 3

Crear un servidor dhcp que se encargue de asignar direcciones IP a todas las máquinas de las diferentes subredes. El servidor dhcp debe identificar de que subred viene la solicitud de asignación y debe asignar una dirección IP correcta para la subred.  