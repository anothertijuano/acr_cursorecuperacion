#!/bin/sh
#Script para generar interface virtual tap0

sudo tunctl -u $1
sudo ifconfig tap0 1.1.0.1 netmask 255.255.255.252
sudo ifconfig tap0 up
sudo route add -net 1.1.1.0/24 gw 1.1.0.2 dev tap0
sudo route add -net 1.1.3.0/30 gw 1.1.0.2 dev tap0
sudo route add -net 1.1.2.0/24 gw 1.1.0.2 dev tap0
