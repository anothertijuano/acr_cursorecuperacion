from os import system, path, environ
dirname = path.dirname(__file__)
filename = path.join(dirname, '$TABLES/libs')
libs=str(environ['TABLES'])+"/libs"
logs=str(environ['TABLES'])+"/logs"
from sys import path
path.append(libs)


def file2list(path):
    fp=open(path,"r")
    LIST=fp.read().split('\n')[:-1]
    fp.close()
    return LIST

def getUnresponsiveHosts():
    hosts=file2list(logs+"/inactiveHosts.out")
    return hosts

def getResponsiveHosts():
    hosts=getHosts()
    unresponsive=getUnresponsiveHosts()
    responsive=[]
    for ip in hosts:
        if ip not in unresponsive:
            responsive.append(ip)
    return responsive

def getHosts():
    hosts=file2list(libs+"/hostnames")
    return hosts

def ping(ip):
    #return 0 if ip is active
    cmd="ping -c 3 "+str(ip)+" > /dev/null"
    code=system(cmd)
    return(code)

