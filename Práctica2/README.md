# Práctica 2

Implementar un servidor TFTP que se encargue de respaldar cada uno de los archivos de configuración de los routers. Para ello, en el anfitrión se creara un script que se conectara a cada uno de los routers para solicitarle su archivo de configuración, el script creara una carpeta que le asignara la IP del router y dentro almacenara el archivo de configuración.